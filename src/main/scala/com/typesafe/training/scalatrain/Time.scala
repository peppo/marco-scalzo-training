package com.typesafe.training.scalatrain

case class Time(hours: Int = 0, minutes: Int = 0) {

  require(hours >= 0 && hours <= 23, "The hours must be within 0 and 23")
  require(minutes >= 0 && minutes <= 59, "The minutes must be within 0 and 59")

  val asMinutes: Int = hours * 60 + minutes

  def minus(that: Time): Int = this.asMinutes - that.asMinutes

  def -(that: Time): Int = this.minus(that)
}

object Time {

  def fromMinute(minutes: Int): Time = Time(minutes / 60, minutes % 60)
}

